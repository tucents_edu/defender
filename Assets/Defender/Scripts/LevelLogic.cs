﻿using UnityEngine;

public class LevelLogic : MonoBehaviour
{
    public int level = 1;

    private void OnCollisionStay(Collision collision)
    {
        if (GameManager.gameManager.isGameOver() && PanelCanvasController.instance.Level != level && collision.transform.tag.Contains("Tip"))
        {
            PanelCanvasController.instance.Level = level;
            PanelCanvasController.instance.SetText("Level " + level + " selected.");
        }
    }
}
