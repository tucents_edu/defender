﻿using System.Collections;
using UnityEngine;

public class ElevatorController : MonoBehaviour
{
    public static ElevatorController instance;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
    }

    public void RaiseElevator()
    {
        ResetElevator();
        GetComponent<AudioSource>().Play();
        StartCoroutine(raise_coroutine());
    }

    public void ResetElevator()
    {
        transform.position = new Vector3(12, 5, 12);
    }

    IEnumerator raise_coroutine()
    {
        while (transform.position.y > -20)
        {
            yield return new WaitForSeconds(0.05f);

            transform.Translate(Vector3.forward * -0.2f);
            yield return null;
        }
    }
}
