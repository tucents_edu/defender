﻿using UnityEngine;
using UnityEngine.UI;

public class ScreenFaderController : AbstractFader
{
    private Transform imageTrans, textTrans;
    public static ScreenFaderController instance;

    void Awake()
    {
        instance = this;

        imageTrans = transform.GetChild(0);
        textTrans = transform.GetChild(1);

        //From AbstractFader
        image = imageTrans.gameObject.GetComponent<Image>();
        text = textTrans.gameObject.GetComponent<Text>();
    }
}
