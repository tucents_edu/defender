﻿using UnityEngine;

public class ExitBtnLogic : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (GameManager.gameManager.isGameOver() && collision.transform.tag.Equals("Tip"))
        {
            Application.Quit();
        }
    }
}
