﻿using UnityEngine;

public class GrabGun : MonoBehaviour
{
    private bool isSwitching;
    private GameObject parent;

    public GameObject gun { get; private set; }
    public bool isHoldingGun { get; private set; }
    public OVRInput.Button handTrigger = OVRInput.Button.SecondaryHandTrigger;
    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void Start()
    {
        isHoldingGun = false;
        parent = transform.parent.gameObject;
        gun = transform.GetChild(0).gameObject;
        gun.SetActive(isHoldingGun);
    }

    void Update()
    {
        bool trigger1 = (parent.transform.parent.transform.localRotation.x > 0.6f);
        bool trigger2 = OVRInput.Get(handTrigger);
        bool trigger = trigger1 || trigger2;

        if (parent)
        {
            if (trigger && !isSwitching)
            {
                isSwitching = true;
                isHoldingGun = !isHoldingGun;
                gun.SetActive(isHoldingGun);
                audioSource.Play();
            }

            if (!trigger && isSwitching)
            {
                isSwitching = false;
            }
        }
    }
}
