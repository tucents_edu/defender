﻿using UnityEngine;
using System.Collections;

public class FireLogic : MonoBehaviour
{
    public GameObject shotPrefab;
    public OVRInput.Button triggerButton;
    public float shotSpeed = 8;
    public bool isLeftGun = false;

    private AudioSource[] audioSources;
    private GameObject gunCylinder;

    private GrabGun grabGun;
    private GameObject shot;

    public int coolDownStep = 5;
    public int gunHeatInc = 10;
    bool isGunHot;
    //private int gunHeat = 0;
    private int toHotLimit = 90;
    private int canFireLimit = 50;
    private bool alreadyCoolingDown;

    private int _gunHeat;
    public int gunHeat
    {
        get { return _gunHeat; }
        private set
        {
            _gunHeat = value;
            if (isLeftGun)
                HelmetHUDManager.instance.SetLeftGunHeat(value);
            else
                HelmetHUDManager.instance.SetRightGunHeat(value);
        }
    }

    void Awake()
    {
        audioSources = GetComponents<AudioSource>();
        gunCylinder = transform.GetChild(1).gameObject;
        grabGun = transform.GetComponentInParent<GrabGun>();
    }

    void Update()
    {
        if (OVRInput.GetDown(triggerButton))
        {
            if (isGunTooHot())
            {
                StartCoroutine(CoolDown());
                audioSources[1].Play();
            }
            else
            {
                audioSources[0].Play();
                shot = GameObject.Instantiate(shotPrefab, gunCylinder.transform.position + gunCylinder.transform.forward * 0.5f, transform.rotation);
                shot.GetComponent<Rigidbody>().AddForce(shotSpeed * gunCylinder.transform.forward,ForceMode.Impulse);
                incGunheat();
            }

        }
    }

    IEnumerator CoolDown()
    {
        if (!alreadyCoolingDown)
        {
            alreadyCoolingDown = true;
            while (gunHeat > 0)
            {
                yield return new WaitForSeconds(0.5f);
                gunHeat -= coolDownStep;
                yield return null;
            }
            gunHeat = 0;
            alreadyCoolingDown = false;
        }
    }

    private bool isGunTooHot()
    {
        if (gunHeat > toHotLimit)
        {
            isGunHot = true;
        }
        else
        if (gunHeat < canFireLimit)
        {
            isGunHot = false;
        }
        return isGunHot;
    }

    private void incGunheat()
    {
        StartCoroutine(CoolDown());
        gunHeat += gunHeatInc;
        if (gunHeat > 100) gunHeat = 100;
    }


}
