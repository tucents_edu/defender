﻿using UnityEngine;
using UnityEngine.UI;

public class MessageCanvasController : AbstractFader
{
    private Transform textTrans;
    public static MessageCanvasController instance;

    void Awake()
    {
        instance = this;

        textTrans = transform.GetChild(0);

        //From AbstractFader
        text = textTrans.gameObject.GetComponent<Text>();
    }
}
