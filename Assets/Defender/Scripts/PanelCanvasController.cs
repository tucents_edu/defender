﻿using UnityEngine;
using UnityEngine.UI;

public class PanelCanvasController : AbstractFader
{
    private Transform imageTrans, textTrans;
    public static PanelCanvasController instance;

    internal int Level = 1;
    private AudioSource audiosource;

    void Awake()
    {
        instance = this;

        imageTrans = transform.GetChild(0);
        textTrans = transform.GetChild(1);

        //From AbstractFader
        image = imageTrans.gameObject.GetComponent<Image>();
        text = textTrans.gameObject.GetComponent<Text>();

        audiosource = GetComponent<AudioSource>();
    }

    internal void SetText(string v)
    {
        audiosource.Play();
        text.text = v;
    }
}
