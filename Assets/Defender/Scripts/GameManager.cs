﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GameManager : MonoBehaviour
{
    public GameObject enemyPrefab;
    public float enemyMinRange = 30.0f;  // min range to place enemy
    public float enemyMaxRange = 50.0f;  // max range to place enemy
    public int enemyCount = 2;
    public int enemySpeed = 1;

    public static GameManager gameManager;

    private Game game;

    int[,] difficultyLevels = new int[,] {
        { 2, 1 }, // 2 enemies , speed 1
        { 2, 3 }, // 2 enemies , speed 3
        { 4, 3 }  // 4 enemies , speed 3
    };


    private void Awake()
    {
        gameManager = this;
    }

    void Start()
    {
        WaveCanvasController.instance.Hide();
        FailCanvasController.instance.Hide();
        MessageCanvasController.instance.Hide();
        
        ScreenFaderController.instance.Hide();

        ResetPlayerPosition();
        PanelCanvasController.instance.SetText("Level " + PanelCanvasController.instance.Level + " selected.");

    }

    void ResetPlayerPosition()
    {
        GameObject.Find("Player").transform.position = new Vector3(12f, 6.6f, 12f);
        GameObject.Find("Player").transform.rotation = Quaternion.Euler(0.0f, -135.0f, 0.0f);
        ElevatorController.instance.ResetElevator();
    }


    internal void StartGame()
    {
        PanelCanvasController.instance.Hide();
        int index = PanelCanvasController.instance.Level - 1;
        int enemyCount = difficultyLevels[index, 0];
        int enemySpeed = difficultyLevels[index, 1];
        ElevatorController.instance.RaiseElevator();

        game = new Game(enemyPrefab, enemyMinRange, enemyMaxRange, enemyCount, enemySpeed);
        game.StartGame();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void EnemyGone()
    {
        game.DecreaseNoEnemiesInWave();
        game.IsThereTimeForANewWave();
    }

    public void EnemyEscaped()
    {
        game.EnemyEscaped();
        EnemyGone();
    }

    public void EnemyKilled()
    {
        game.EnemyKilled();
        EnemyGone();
    }

    internal bool isGameOver()
    {
        return game == null || game.isGameOver();
    }



    class Game
    {
        public GameObject enemyPrefab;
        public float enemyMinRange = 30.0f;  // min range to place enemy
        public float enemyMaxRange = 50.0f;  // max range to place enemy
        public int enemyCount = 2;
        public int enemySpeed = 1;

        private int waveNo = 1;
        private int noEnemiesInWave;

        private bool gameOver = true;
        private int score;

        private int enemiesLeftToEscape;


        public Game(GameObject enemyPrefab, float enemyMinRange, float enemyMaxRange, int enemyCount, int enemySpeed)
        {
            this.enemyPrefab = enemyPrefab;
            this.enemyMinRange = enemyMinRange;
            this.enemyMaxRange = enemyMaxRange;
            this.enemyCount = enemyCount;
            this.enemySpeed = enemySpeed;
            enemiesLeftToEscape = 5;

        }

        private void StartNewWave()
        {
            noEnemiesInWave = enemyCount * waveNo;

            HelmetHUDManager.instance.SetWave(waveNo);
            HelmetHUDManager.instance.SetEnemies(noEnemiesInWave);
            HelmetHUDManager.instance.SetEnemiesLeft(enemiesLeftToEscape);

            WaveCanvasController.instance.FadeIn("Wave: " + waveNo);
            WaveCanvasController.instance.FadeOut(3.0f);
            GameManager.gameManager.StartCoroutine(SpawnWave());

        }

        private IEnumerator SpawnWave()
        {
            yield return new WaitForSeconds(5);

            for (int i = 0; i < noEnemiesInWave; i++)
            {
                float angle = UnityEngine.Random.Range(0, 2 * Mathf.PI);
                float dist = UnityEngine.Random.Range(enemyMinRange, enemyMaxRange);

                Vector3 pos = new Vector3(dist * Mathf.Cos(angle), 5, dist * Mathf.Sin(angle));
                GameObject enemy = GameObject.Instantiate(enemyPrefab, pos, Quaternion.Euler(0, 0, 0));

                enemy.GetComponent<NavMeshAgent>().speed = this.enemySpeed;
                yield return null;
            }
        }


        internal void StartGame()
        {
            gameOver = false;
            score = 0;
            StartNewWave();
        }

        internal void DecreaseNoEnemiesInWave()
        {
            noEnemiesInWave--;
            HelmetHUDManager.instance.SetEnemies(noEnemiesInWave);
        }

        internal void IsThereTimeForANewWave()
        {
            if (gameOver)
                return;

            if (noEnemiesInWave < 1)
            {
                waveNo++;
                StartNewWave();
            }
        }

        internal void EnemyEscaped()
        {
            enemiesLeftToEscape--;
            HelmetHUDManager.instance.SetEnemiesLeft(enemiesLeftToEscape);
            if (enemiesLeftToEscape == 0)
            {
                gameOver = true;
                RemoveAllEnemies();
                FailCanvasController.instance.FadeIn("Mission Failed", 32);
                FailCanvasController.instance.FadeOut(4.0f);
                ScreenFaderController.instance.FadeIn(() => {
                    PanelCanvasController.instance.FadeIn("");
                    gameManager.ResetPlayerPosition();
                }, "", 6, 3.0f);
                ScreenFaderController.instance.FadeOut(8);
            }
        }

        internal void EnemyKilled()
        {
            score++;
            HelmetHUDManager.instance.SetScore(score);
        }

        internal bool isGameOver()
        {
            return gameOver;
        }

        private void RemoveAllEnemies()
        {
            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in enemies)
            {
                Destroy(enemy);
            }
        }

    }
}
