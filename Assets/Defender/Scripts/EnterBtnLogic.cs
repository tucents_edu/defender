﻿using UnityEngine;

public class EnterBtnLogic : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (GameManager.gameManager.isGameOver() && collision.transform.tag.Equals("Tip"))
        {
            GameManager.gameManager.StartGame();
        }
    }
}
