﻿using UnityEngine;
using UnityEngine.UI;

public class WaveCanvasController : AbstractFader
{
    private Transform imageTrans, textTrans;
    public static WaveCanvasController instance;
    private AudioSource audioSource;

    void Awake()
    {
        instance = this;

        imageTrans = transform.GetChild(0);
        textTrans = transform.GetChild(1);

        //From AbstractFader
        image = imageTrans.gameObject.GetComponent<Image>();
        text = textTrans.gameObject.GetComponent<Text>();

        audioSource = GetComponent<AudioSource>();
    }

    public new void FadeIn(string msg = "", int fontSize = 72, float enterDelay = 0f,
        float interDelay = 0.0025f)
    {
        audioSource.Play();
        base.FadeIn(msg, fontSize, enterDelay, interDelay);
    }

}
