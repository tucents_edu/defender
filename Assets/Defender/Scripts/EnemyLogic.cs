﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyLogic : MonoBehaviour
{
    private Animator animator;
    private Transform healthBar;
    private GameObject enemyDeathEffect;
    int enemyHealthPoint = 5;

    private static Color green = new Vector4(0.0f, 1.0f, 0.0f, 0.3f);
    private static Color lightgreen = new Vector4(0.83f, 1.0f, 0.0f, 0.3f);
    private static Color yellow = new Vector4(1.0f, 1.0f, 0.0f, 0.3f);
    private static Color orange = new Vector4(1.0f, 0.69f, 0.0f, 0.3f);
    private static Color red = new Vector4(1.0f, 0.0f, 0.0f, 0.3f);
    private static Color[] col = { red, orange, yellow, lightgreen, green };

    public NavMeshAgent agent;
    private bool enemyAlive = true;
    private GameManager gameManager;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.Warp(transform.position); //Needed for the agent to know where it is
        agent.SetDestination(Vector3.zero);
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }


    // Start is called before the first frame update
    void Start()
    {
        animator = transform.GetChild(0).GetComponent<Animator>();
        enemyDeathEffect = transform.GetChild(1).gameObject;

        healthBar = gameObject.transform.Find("HealthBar");
        healthBar.GetComponent<Renderer>().material.color = col[enemyHealthPoint - 1];
        healthBar.localScale = new Vector3(0.2f, enemyHealthPoint / 20f, 0.2f);
        healthBar.Translate(Vector3.down * enemyHealthPoint / 40f);

    }

    // Update is called once per frame
    void Update()
    {
        // Is the enemy close enought to the center of the compound
        if (Vector3.Distance(transform.position, Vector3.zero) < 1.5f)
        {
            if (enemyAlive)
            {
                enemyAlive = false;
                enemyDeathEffect.SetActive(true);
                gameManager.EnemyEscaped();
                Destroy(gameObject, 0.2f);
            }
        }

    }

    internal void gotHit()
    {
        enemyHealthPoint--;

        if (enemyHealthPoint > 0)
        {
            healthBar.GetComponent<Renderer>().material.color = col[enemyHealthPoint - 1];
            healthBar.localScale = new Vector3(0.2f, enemyHealthPoint / 20f, 0.2f);
        }
        else if (enemyHealthPoint == 0)
        {
            GetComponent<AudioSource>().Play();
            healthBar.localScale = Vector3.zero;
            agent.speed = 0;
            enemyDeathEffect.GetComponent<ParticleSystem>().Play();
            animator.SetBool("die", true);
            gameManager.EnemyKilled();
            Destroy(gameObject, 0.5f);
        }
    }

}
