﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HelmetHUDManager : MonoBehaviour
{
    public static HelmetHUDManager instance;
    private Text WaveText;
    private Text EnemiesText;
    private Text ScoreText;
    private Slider EnemiesLeftSlider;
    private Slider LeftGunHeatSlider;
    private Slider RightGunHeatSlider;

    private void Awake()
    {
        instance = this;
        WaveText = GameObject.Find("WaveText").GetComponent<Text>();
        EnemiesText = GameObject.Find("EnemiesText").GetComponent<Text>();
        ScoreText = GameObject.Find("ScoreText").GetComponent<Text>();
        EnemiesLeftSlider = GameObject.Find("EnemiesLeftSlider").GetComponent<Slider>();
        LeftGunHeatSlider = GameObject.Find("LeftGunHeatSlider").GetComponent<Slider>();
        RightGunHeatSlider = GameObject.Find("RightGunHeatSlider").GetComponent<Slider>();

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetWave(int waveNo)
    {
        WaveText.text = "Wave: " + waveNo;
    }

    public void SetEnemies(int enemiesNo)
    {
        EnemiesText.text = "Enemies: " + enemiesNo;
    }

    public void SetScore(int score)
    {
        ScoreText.text = "Score: " + score;
    }

    public void SetEnemiesLeft(int v)
    {
        EnemiesLeftSlider.value = v;
    }
    public void SetLeftGunHeat(int v)
    {
        LeftGunHeatSlider.value = v;
    }

    public void SetRightGunHeat(int v)
    {
        RightGunHeatSlider.value = v;
    }

}
