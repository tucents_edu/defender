﻿using UnityEngine;

public class ShotLogic : MonoBehaviour
{
    private GameObject hit;
    private GameObject bullet;
    private Rigidbody shotRigidbody;
    private bool alreadyHit;

    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, 4);
        bullet = transform.GetChild(0).gameObject;
        hit = transform.GetChild(1).gameObject;
        shotRigidbody = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (alreadyHit)
            return;
        alreadyHit = true;

        if (shotRigidbody)
        {
            GetComponent<Rigidbody>().detectCollisions = false;
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().useGravity = false;
        }

        //If we hit an enemy we call gotHit on it.
        if (collision.gameObject.tag == "Enemy")
        {
            collision.gameObject.GetComponent<EnemyLogic>().gotHit();
        }

        bullet.SetActive(false);
        hit.GetComponent<ParticleSystem>().Play();

        Destroy(gameObject, 0.5f);
    }
}
