﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;

public abstract class AbstractFader : MonoBehaviour
{
    internal Text text;
    internal Image image;
    private Color icolor;
    private Color tcolor;

    public void Hide()
    {
        tcolor = text.color;
        tcolor.a = 0;
        text.color = tcolor;

        if (image != null)
        {
            icolor = image.color;
            icolor.a = 0;
            image.color = icolor;
        }
    }

    public void FadeOut(float enterDelay = 0f, float interDelay = 0.0025f)
    {
        Assert.IsNotNull(text);
        StartCoroutine(_FadeOut(enterDelay, interDelay));
    }

    public void FadeOut(System.Action endAction = null, float enterDelay = 0f, float interDelay = 0.0025f)
    {
        Assert.IsNotNull(text);
        StartCoroutine(_FadeOut(enterDelay, interDelay, endAction));
    }

    public void FadeIn(string msg = "", int fontSize = 72, float enterDelay = 0f, float interDelay = 0.0025f)
    {
        Assert.IsNotNull(text);
        StartCoroutine(_FadeIn(enterDelay, interDelay, msg, fontSize));
    }

    public void FadeIn(System.Action endAction = null, string msg = "", int fontSize = 72, float enterDelay = 0f, float interDelay = 0.0025f)
    {
        Assert.IsNotNull(text);
        StartCoroutine(_FadeIn(enterDelay, interDelay, msg, fontSize, endAction));
    }

    private IEnumerator _FadeOut(float enterDelay, float interDelay,System.Action action = null)
    {
        yield return new WaitForSeconds(enterDelay);

        tcolor = text.color;
        icolor = tcolor;
        if (image != null) icolor= image.color;
        float alpha = text.color.a;


        while (alpha > 0)
        {
            yield return new WaitForSeconds(interDelay);
            alpha -= 0.01f;
            icolor.a = alpha;
            if (image != null) image.color = icolor;
            tcolor.a = alpha;
            text.color = tcolor;
            yield return null;
        }

        if (action != null) action();
    }

    private IEnumerator _FadeIn(float enterDelay, float interDelay, string msg, int fontSize, System.Action action = null)
    {
        tcolor = text.color;
        tcolor.a = 0;
        text.color = tcolor;

        icolor = tcolor;
        if (image != null)
        {
            icolor = image.color;
            icolor.a = 0;
            image.color = icolor;
        }

        float alpha = 0;
        text.text = msg;
        text.fontSize = fontSize;

        yield return new WaitForSeconds(enterDelay);
        while (alpha < 1)
        {
            yield return new WaitForSeconds(interDelay);
            alpha += 0.01f;
            icolor.a = alpha;
            if (image != null) image.color = icolor;
            tcolor.a = alpha;
            text.color = tcolor;
            yield return null;
        }

        if (action != null) action();
    }
}
