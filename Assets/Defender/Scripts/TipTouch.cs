﻿using UnityEngine;

public class TipTouch : MonoBehaviour
{
    private const string LEFT_INDEX = "hands:b_l_index_ignore";
    private const string RIGHT_INDEX = "hands:b_r_index_ignore";
    Transform indexTransform;

    // Update is called once per frame
    void Update()
    {
        if (transform.parent.name.StartsWith("Left"))
        {
            indexTransform = GameObject.Find(LEFT_INDEX).transform;
        }
        else
        {
            indexTransform = GameObject.Find(RIGHT_INDEX).transform;
        }
        transform.position = indexTransform.position;
    }
}
